use crate::config::CONFIG;
use crate::errors::ApiError;
use actix_web::web;
use diesel::pg::PgConnection;
use diesel::r2d2::{ConnectionManager, Pool, PooledConnection};

pub type PgPool = Pool<ConnectionManager<PgConnection>>;
pub type PgPooledConnection = PooledConnection<ConnectionManager<PgConnection>>;

pub fn establish_connection() -> PgPool {
    let manager = ConnectionManager::<PgConnection>::new(&CONFIG.database_url);
    Pool::builder()
        .build(manager)
        .expect("Failed to create pool")
}

pub fn pg_pool_handler(pool: web::Data<PgPool>) -> Result<PgPooledConnection, ApiError> {
    pool.get()
        .map_err(|e| ApiError::InternalServerError(e.to_string()))
}

///测试
#[cfg(test)]
mod tests {
    use super::*;
    use crate::models::user::User;
    use crate::schema::users::dsl::*;
    use diesel::prelude::*;
    use diesel::QueryDsl;

    #[test]
    fn test_disel() {
        let pool = establish_connection();
        let conn = &*pool.get().expect("couldn't get db connection from pool");
        let user_id = "00000000-0000-0000-0000-000000000000";
        let results = users
            .filter(id.eq(user_id))
            .limit(1)
            .load::<User>(conn)
            .expect("Error loading users");
        assert!(results[0].id == user_id)
    }
}

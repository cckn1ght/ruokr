use actix_web::web;

use crate::handlers::{auth, okr, user};
use crate::handlers::health::get_health;
use crate::middleware::auth::Auth as AuthMiddleware;

pub fn routes(cfg: &mut web::ServiceConfig) {
    cfg.route("/health", web::get().to(get_health))
        // /api/v1 routes
        .service(
            web::scope("/api/v1")
                .wrap(AuthMiddleware)
                .service(
                    web::scope("/auth")
                        .route("/login", web::post().to(auth::login))
                        .route("/status", web::get().to(auth::status))
                        .route("/logout", web::get().to(auth::logout)),
                )
                .service(
                    web::scope("/user")
                        .route("/register", web::post().to(user::create_user))
                        .route("", web::get().to(user::get_all_users))
                        .route("/{id}", web::get().to(user::get_user))
                        .route("/{id}", web::put().to(user::update_user))
                        .route("/{id}", web::delete().to(user::delete_user)),
                )
                .service(
                    web::scope("/okr")
                        .route("", web::post().to(okr::create_okr_handler))
                        .route("", web::get().to(okr::get_all_okrs_handler))
                        .route("/by-owner", web::get().to(okr::get_okrs_by_owner_id_handler))
                        .route("/{okr_id}", web::get().to(okr::get_okr_by_id_handler))
                        .route("/{okr_id}", web::put().to(okr::update_okr_handler))
                        .route("/{okr_id}", web::delete().to(okr::delete_okr_handler))
                ),
        );
}

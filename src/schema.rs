table! {
    okrs (id) {
        id -> Varchar,
        owner_id -> Varchar,
        owner_name -> Varchar,
        title -> Varchar,
        obj -> Varchar,
        created_at -> Timestamp,
        krs_json -> Nullable<Jsonb>,
    }
}

table! {
    users (id) {
        id -> Varchar,
        name -> Varchar,
        username -> Varchar,
        email -> Varchar,
        password -> Varchar,
        created_at -> Timestamp,
    }
}

joinable!(okrs -> users (owner_id));

allow_tables_to_appear_in_same_query!(
    okrs,
    users,
);

use crate::errors::ApiError;
use crate::helpers::respond_json;
use actix_web::HttpResponse;

#[derive(Debug, Deserialize, Serialize, PartialEq)]
pub struct HealthResponse {
    pub status: String,
    pub version: String,
}

/// Handler to get the liveness of the service
pub async fn get_health() -> Result<HttpResponse, ApiError> {
    respond_json(HealthResponse {
        status: "ok".into(),
        version: env!("CARGO_PKG_VERSION").into(),
    })
}

#[cfg(test)]
mod tests {
    use super::*;
    use actix_http::body::{ResponseBody, Body};

    #[actix_rt::test]
    async fn test_get_health() {
        let response: HttpResponse = get_health().await.unwrap();
        if let ResponseBody::Body(b) = response.body() {
            if let Body::Bytes(by) = b {
                let s = String::from_utf8(by.to_vec()).unwrap();
                let res: HealthResponse = serde_json::from_str(s.as_str()).unwrap();
                assert_eq!(res.status, "ok".to_string());
            }
        }
        // assert_eq!(response.body().status, "ok".to_string());
    }
}

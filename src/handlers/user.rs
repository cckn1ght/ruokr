use crate::database::{pg_pool_handler, PgPool};
use crate::errors::ApiError;
use crate::helpers::{respond_json, respond_ok};
use crate::models::user::{create, delete, find, get_all, update, NewUser, UpdateUser, User};
use crate::validate::validate;
use actix_web::web::{block, Data, HttpResponse, Json, Path};
use uuid::Uuid;
use validator::Validate;

#[derive(Clone, Debug, Deserialize, Serialize, Validate)]
pub struct CreateUserRequest {
    #[validate(length(
        min = 2,
        message = "name is required and must be at least 2 characters"
    ))]
    pub name: String,

    #[validate(length(
    min = 2,
    message = "username is required and must be at least 2 characters"
    ))]
    pub username: String,

    #[validate(email(message = "email must be a valid email"))]
    pub email: String,

    #[validate(length(
        min = 1,
        message = "password is required and must be at least 1 characters"
    ))]
    pub password: String,
}
#[derive(Clone, Debug, Deserialize, Serialize, Validate)]
pub struct UpdateUserRequest {
    #[validate(length(
        min = 2,
        message = "name is required and must be at least 2 characters"
    ))]
    pub name: String,

    #[validate(email(message = "email must be a valid email"))]
    pub email: String,
}
/// Finds user by UID.
pub async fn get_user(user_id: Path<Uuid>, pool: Data<PgPool>) -> Result<HttpResponse, ApiError> {
    let conn = pg_pool_handler(pool)?;
    let user = block(move || find(&conn, *user_id)).await?;
    respond_json(user)
}

/// get all users
pub async fn get_all_users(pool: Data<PgPool>) -> Result<HttpResponse, ApiError> {
    let conn = pg_pool_handler(pool)?;
    let users = block(move || get_all(&conn)).await?;
    respond_json(users)
}

/// Create a user
pub async fn create_user(
    pool: Data<PgPool>,
    params: Json<CreateUserRequest>,
) -> Result<HttpResponse, ApiError> {
    validate(&params)?;
    let conn = pg_pool_handler(pool)?;

    // temporarily use the new user's id for created_at/updated_at
    // update when auth is added
    let user_id = Uuid::new_v4();
    let new_user: User = NewUser {
        id: user_id.to_string(),
        name: params.name.to_string(),
        username: params.username.to_string(),
        email: params.email.to_string(),
        password: params.password.to_string(),
    }
        .into();
    let user = block(move || create(&conn, &new_user)).await?;
    respond_json(user)
}

/// update an user
pub async fn update_user(
    user_id: Path<Uuid>,
    pool: Data<PgPool>,
    params: Json<UpdateUserRequest>,
) -> Result<HttpResponse, ApiError> {
    validate(&params)?;
    let conn = pg_pool_handler(pool)?;
    let user: UpdateUser = UpdateUser {
        id: user_id.to_string(),
        name: params.name.to_string(),
        email: params.email.to_string(),
    };
    let user = block(move || update(&conn, &user)).await?;
    respond_json(user)
}
/// deletes an user
pub async fn delete_user(
    user_id: Path<Uuid>,
    pool: Data<PgPool>,
) -> Result<HttpResponse, ApiError> {
    let conn = pg_pool_handler(pool)?;
    block(move || delete(&conn, *user_id)).await?;
    respond_ok()
}

use actix_identity::{Identity, RequestIdentity};
use actix_web::web::{block, Data, HttpResponse, Json};

use crate::auth::{create_jwt, PrivateClaim, decode_jwt};
use crate::database::{pg_pool_handler, PgPool};
use crate::errors::ApiError;
use crate::helpers::{respond_json, respond_ok};
use crate::models::user::{find_by_auth, UserResponse};
use crate::validate::validate;
use actix_web::HttpRequest;

#[derive(Clone, Debug, Deserialize, Serialize, Validate)]
pub struct LoginRequest {
    #[validate(length(
    min = 1,
    message = "username is required and must be at least 1 characters"
    ))]
    pub username: String,

    #[validate(length(
        min = 4,
        message = "password is required and must be at least 2 characters"
    ))]
    pub password: String,
}
#[derive(Debug, Deserialize, Serialize, PartialEq)]
pub struct LoginResponse {
    user: UserResponse,
    jwt: String,
}
/// Login a user
/// Create and remember their JWT
pub async fn login(
    id: Identity,
    pool: Data<PgPool>,
    params: Json<LoginRequest>,
) -> Result<HttpResponse, ApiError> {
    validate(&params)?;
    let conn = pg_pool_handler(pool)?;

    // Validate that the email + hashed password matches
    let user = block(move || find_by_auth(&conn, &params.username, &params.password)).await?;

    // Create a JWT
    let private_claim = PrivateClaim::new(user.id, user.name.clone(), user.username.clone(), user.email.clone());
    let jwt = create_jwt(&private_claim)?;

    // Remember the token
    id.remember(jwt.clone());
    respond_json(LoginResponse {user, jwt})
}

/// Create and remember their JWT
pub async fn status(
    id: Identity,
    req: HttpRequest,
) -> Result<HttpResponse, ApiError> {
    let identity = RequestIdentity::get_identity(&req).unwrap_or("".into());
    let private_claim: Result<PrivateClaim, ApiError> = decode_jwt(&identity);
    let is_logged_in = private_claim.is_ok();
    if is_logged_in {
        // refresh jwt
        let p = private_claim.unwrap();
        let jwt = create_jwt(&p)?;
        // Remember the token
        id.remember(jwt);
        let u: UserResponse = p.into();
        respond_json(u)
    } else {
        Result::Err(ApiError::Unauthorized("unauthorized".into()))
    }
}

/// Logout a user
/// Forget their user_id
pub async fn logout(id: Identity) -> Result<HttpResponse, ApiError> {
    id.forget();
    respond_ok()
}

#[cfg(test)]
pub mod tests {
    use actix_identity::Identity;
    use actix_web::{FromRequest, test};

    use crate::database::establish_connection;

    use super::*;

    fn get_data_pool() -> Data<PgPool> {
        Data::new(establish_connection())
    }

    async fn get_identity() -> Identity {
        let (request, mut payload) =
            test::TestRequest::with_header("content-type", "application/json").to_http_parts();
        let identity = Option::<Identity>::from_request(&request, &mut payload)
            .await
            .unwrap()
            .unwrap();
        identity
    }

    async fn login_user() -> Result<HttpResponse, ApiError> {
        let params = LoginRequest {
            username: "test".into(),
            password: "password".into(),
        };
        let identity = get_identity().await;
        login(identity, get_data_pool(), Json(params)).await
    }

    async fn logout_user() -> Result<HttpResponse, ApiError> {
        let identity = get_identity().await;
        logout(identity).await
    }

    #[actix_rt::test]
    async fn it_logs_a_user_in() {
        let response = login_user().await;
        assert!(response.is_ok());
    }

    #[actix_rt::test]
    async fn it_logs_a_user_out() {
        login_user().await.unwrap();
        let response = logout_user().await;
        assert!(response.is_ok());
    }
}

use actix_web::web::{block, Data, HttpResponse, Json, Path, Query};
use chrono::Utc;
use uuid::Uuid;
use validator::Validate;

use crate::database::{pg_pool_handler, PgPool};
use crate::errors::ApiError;
use crate::helpers::{respond_json, respond_ok};
use crate::models::okr::{create_okr, delete_okr, find_okr_by_id, find_okr_by_owner_id, get_all_okrs, update_okr, Okr, UpdateOkr, KrsJson};
use crate::validate::validate;

#[derive(Clone, Debug, Deserialize, Serialize, Validate)]
pub struct CreateOkrRequest {
    pub owner_id: Uuid,
    #[validate(length(
    min = 2,
    message = "owner name is required"
    ))]
    pub owner_name: String,

    #[validate(length(
    min = 2,
    message = "title is required and must be at least 2 characters"
    ))]
    pub title: String,

    #[validate(length(
    min = 2,
    message = "objective is required and must be at least 2 characters"
    ))]
    pub obj: String,

    pub krs_json: Option<KrsJson>
}

#[derive(Clone, Debug, Deserialize, Serialize, Validate)]
pub struct UpdateOkrRequest {

    #[validate(length(
    min = 2,
    message = "title is required and must be at least 2 characters"
    ))]
    pub title: String,

    #[validate(length(
    min = 2,
    message = "objective is required and must be at least 2 characters"
    ))]
    pub obj: String,

    pub krs_json: Option<KrsJson>
}

/// get all okrs
pub async fn get_all_okrs_handler(pool: Data<PgPool>) -> Result<HttpResponse, ApiError> {
    let conn = pg_pool_handler(pool)?;
    let okr = block(move || get_all_okrs(&conn)).await?;
    respond_json(okr)
}
/// get an okr by id
pub async fn get_okr_by_id_handler(okr_id: Path<Uuid>, pool: Data<PgPool>) -> Result<HttpResponse, ApiError> {
    let conn = pg_pool_handler(pool)?;
    let okr = block(move || find_okr_by_id(&conn, *okr_id)).await?;
    respond_json(okr)
}

#[derive(Deserialize)]
pub struct ByOwnerId {
    owner_id: Uuid
}
/// get okrs by owner
pub async fn get_okrs_by_owner_id_handler(query: Query<ByOwnerId>, pool: Data<PgPool>) -> Result<HttpResponse, ApiError> {
    let conn = pg_pool_handler(pool)?;
    let okr = block(move || find_okr_by_owner_id(&conn, query.owner_id)).await?;
    respond_json(okr)
}

/// create an okr
pub async fn create_okr_handler(
    pool: Data<PgPool>,
    params: Json<CreateOkrRequest>,
) -> Result<HttpResponse, ApiError> {
    validate(&params)?;
    let conn = pg_pool_handler(pool)?;
    let new_okr = Okr {
        id: Uuid::new_v4().to_string(),
        owner_id: params.owner_id.to_string(),
        owner_name: params.owner_name.to_string(),
        title: params.title.to_string(),
        obj: params.obj.to_string(),
        created_at: Utc::now().naive_utc(),
        krs_json: KrsJson::struct_to_option_json_value(params.krs_json.clone()),
    };
    let okr = block(move || create_okr(&conn, &new_okr)).await?;
    respond_json(okr)
}

/// update an okr
pub async fn update_okr_handler(
    okr_id: Path<Uuid>,
    pool: Data<PgPool>,
    params: Json<UpdateOkrRequest>,
) -> Result<HttpResponse, ApiError> {
    validate(&params)?;
    let conn = pg_pool_handler(pool)?;
    let okr: UpdateOkr = UpdateOkr {
        id: okr_id.to_string(),
        title: params.title.to_string(),
        obj: params.obj.to_string(),
        krs_json: KrsJson::struct_to_option_json_value(params.krs_json.clone()),
    };
    let res = block(move || update_okr(&conn, &okr)).await?;
    respond_json(res)
}
/// deletes an okr
pub async fn delete_okr_handler(
    okr_id: Path<Uuid>,
    pool: Data<PgPool>,
) -> Result<HttpResponse, ApiError> {
    let conn = pg_pool_handler(pool)?;
    block(move || delete_okr(&conn, *okr_id)).await?;
    respond_ok()
}

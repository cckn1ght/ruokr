use actix_web::{middleware, App, HttpServer};

use crate::auth::get_identity_service;
use crate::config::CONFIG;
use crate::database;
use crate::routes::routes;

pub async fn server() -> std::io::Result<()> {
    dotenv::dotenv().ok();
    std::env::set_var("RUST_BACKTRACE", "1");
    env_logger::init();
    let pool = database::establish_connection();
    //http路由
    HttpServer::new(move || {
        App::new()
            .wrap(middleware::Logger::default())
            .wrap(get_identity_service())
            .data(pool.clone())
            .configure(routes)
    })
    .bind(&CONFIG.server)?
    .run()
    .await
}

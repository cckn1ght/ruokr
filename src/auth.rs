use bcrypt::{hash, verify};
use chrono::{Duration, Utc};
use jsonwebtoken::{decode, encode, DecodingKey, EncodingKey, Header, Validation};
use uuid::Uuid;

use crate::config::CONFIG;
use crate::errors::ApiError;
use actix_identity::{CookieIdentityPolicy, IdentityService};

#[derive(Clone, Debug, Serialize, Deserialize, PartialEq)]
pub struct PrivateClaim {
    pub user_id: Uuid,
    pub name: String,
    pub username: String,
    pub email: String,
    exp: i64,
}

impl PrivateClaim {
    pub fn new(user_id: Uuid, name: String, username: String, email: String) -> Self {
        Self {
            user_id,
            name,
            username,
            email,
            exp: (Utc::now() + Duration::hours(CONFIG.jwt_expiration)).timestamp(),
        }
    }
}

/// Create a json web token (JWT)
pub fn create_jwt(private_claim: &PrivateClaim) -> Result<String, ApiError> {
    let encoding_key = EncodingKey::from_secret(&CONFIG.jwt_key.as_ref());
    encode(&Header::default(), private_claim, &encoding_key)
        .map_err(|e| ApiError::CannotEncodeJwtToken(e.to_string()))
}

/// Decode a json web token (JWT)
pub fn decode_jwt(token: &str) -> Result<PrivateClaim, ApiError> {
    let decoding_key = DecodingKey::from_secret(&CONFIG.jwt_key.as_ref());
    decode::<PrivateClaim>(token, &decoding_key, &Validation::default())
        .map(|data| data.claims)
        .map_err(|e| ApiError::CannotDecodeJwtToken(e.to_string()))
}

/// Gets the identidy service for injection into an Actix app
pub fn get_identity_service() -> IdentityService<CookieIdentityPolicy> {
    IdentityService::new(
        CookieIdentityPolicy::new(&CONFIG.session_key.as_ref())
            .name(&CONFIG.session_name)
            .max_age_time(time::Duration::hours(CONFIG.session_timeout))
            .secure(CONFIG.session_secure),
    )
}

/// Encrypt a password
///
/// Uses the argon2i algorithm.
/// auth_salt is environment-configured.
pub fn encrypt(password: &str) -> String {
    // 4 is min cost
    hash(password, 4).unwrap()
}
pub fn verify_password(password: &str, hashed: &str) -> bool {
    verify(password, hashed).unwrap()
}

#[cfg(test)]
pub mod tests {
    use super::*;

    static EMAIL: &str = "test@test.com";
    static USERNAME: &str = "test";
    static NAME: &str = "test";

    #[test]
    fn it_hashes_a_password() {
        let password = "password";
        let hashed = encrypt(password);
        assert_ne!(password, hashed);
    }

    #[test]
    fn it_verifies_a_password() {
        let password = "password";
        let hashed = encrypt(password);
        let verified = verify_password(password, hashed.as_str());
        assert!(verified);
    }

    #[test]
    fn it_creates_a_jwt() {
        let private_claim = PrivateClaim::new(Uuid::new_v4(), NAME.into(), USERNAME.into(), EMAIL.into());
        let jwt = create_jwt(&private_claim);
        assert!(jwt.is_ok());
    }

    #[test]
    fn it_decodes_a_jwt() {
        let private_claim = PrivateClaim::new(Uuid::new_v4(), NAME.into(), USERNAME.into(), EMAIL.into());
        let jwt = create_jwt(&private_claim).unwrap();
        let decoded = decode_jwt(&jwt).unwrap();
        assert_eq!(private_claim, decoded);
    }
}

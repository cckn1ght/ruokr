#[macro_use]
extern crate diesel;
#[macro_use]
extern crate lazy_static;
#[macro_use]
extern crate serde_derive;
#[macro_use]
extern crate validator;
#[macro_use]
extern crate log;
#[macro_use]
extern crate serde_json;

extern crate bcrypt;

use crate::server::server;

mod auth;
mod config;
mod database;
mod errors;
mod handlers;
mod helpers;
mod middleware;
mod models;
mod routes;
mod schema;
mod server;
mod validate;

#[actix_rt::main]
async fn main() -> std::io::Result<()> {
    server().await
}

use crate::schema::okrs;
use crate::errors::ApiError;

use uuid::Uuid;
use chrono::NaiveDateTime;
use diesel::PgConnection;
use diesel::prelude::*;
use rayon::prelude::*;


#[derive(Clone, Debug, Deserialize, Serialize, PartialEq)]
pub struct OkrResponse {
    pub id: Uuid,
    pub owner_id: Uuid,
    pub owner_name: String,
    pub title: String,
    pub obj: String,
    pub krs_json: KrsJson,
    pub created_at: NaiveDateTime,
}

#[derive(Debug, Deserialize, Serialize, PartialEq)]
pub struct OkrsResponse(pub Vec<OkrResponse>);

#[derive(
AsChangeset,
Clone,
Debug,
Serialize,
Deserialize,
PartialEq,
Queryable,
Identifiable,
Insertable,
)]
#[table_name = "okrs"]
pub struct Okr {
    pub id: String,
    pub owner_id: String,
    pub owner_name: String,
    pub title: String,
    pub obj: String,
    pub created_at: NaiveDateTime,
    pub krs_json: Option<serde_json::Value>, // this is for type KrsJson
}

#[derive(Clone, Debug, Deserialize, Serialize, PartialEq)]
pub struct KrsJson {
    pub krs: Vec<KrEntry>
}
impl KrsJson {
    pub fn new(contents: Vec<String>) -> Self {
        let krs: Vec<KrEntry> = contents.into_iter().map(|c| KrEntry::new(c.clone())).collect();
        KrsJson { krs }
    }
    pub fn struct_to_option_json_value(krs_json: Option<KrsJson>) -> Option<serde_json::Value> {
        match krs_json {
            None => None,
            Some(v) => Some(json!(v))
        }
    }
}

#[derive(Clone, Debug, Deserialize, Serialize, PartialEq)]
pub struct KrEntry {
    pub content: String,
    pub progress: i32,
}
impl KrEntry {
    pub fn new(content: String) -> Self {
        KrEntry {
            content,
            progress: 0,
        }
    }
}
#[derive(Clone, Debug, Serialize, Deserialize, AsChangeset)]
#[table_name = "okrs"]
pub struct UpdateOkr {
    pub id: String,
    pub title: String,
    pub obj: String,
    pub krs_json: Option<serde_json::Value>,
}

impl From<Okr> for OkrResponse {
    fn from(okr: Okr) -> Self {
        let krs_json: KrsJson = match okr.krs_json {
            None => KrsJson::new(vec![]),
            Some(v) => serde_json::from_value::<KrsJson>(v).unwrap()
        };
        OkrResponse {
            id: Uuid::parse_str(&okr.id).unwrap(),
            owner_id: Uuid::parse_str(&okr.owner_id).unwrap(),
            owner_name: okr.owner_name.to_string(),
            title: okr.title.to_string(),
            obj: okr.obj.to_string(),
            krs_json,
            created_at: okr.created_at,
        }
    }
}

impl From<Vec<Okr>> for OkrsResponse {
    fn from(okrs: Vec<Okr>) -> Self {
        OkrsResponse(okrs.into_par_iter().map(|okr| okr.into()).collect())
    }
}

// Get all okrs
pub fn get_all_okrs(conn: &PgConnection) -> Result<OkrsResponse, ApiError> {
    use crate::schema::okrs::dsl::*;
    let all_okrs = okrs.load::<Okr>(conn)?;
    Ok(all_okrs.into())
}

pub fn find_okr_by_id(conn: &PgConnection, okr_id: Uuid) -> Result<OkrResponse, ApiError> {
    use crate::schema::okrs::dsl::*;
    let res: Okr = okrs.find(okr_id.to_string()).first(conn)?;
    Ok(res.into())
}
pub fn find_okr_by_owner_id(conn: &PgConnection, oid: Uuid) -> Result<OkrsResponse, ApiError> {
    use crate::schema::okrs::dsl::*;
    let all_okrs: Vec<Okr> = okrs.filter(owner_id.eq(oid.to_string())).load::<Okr>(conn)?;
    Ok(all_okrs.into())
}

pub fn create_okr(conn: &PgConnection, okr: &Okr) -> Result<OkrResponse, ApiError> {
    let o: Okr = diesel::insert_into(okrs::table)
        .values(okr)
        .get_result(conn)?;
    Ok(o.into())
}
pub fn update_okr(conn: &PgConnection, okr: &UpdateOkr) -> Result<OkrResponse, ApiError> {
    use crate::schema::okrs::dsl::*;
    let o = diesel::update(okrs.find(&okr.id.clone()))
        .set(okr)
        .get_result::<Okr>(conn)?;
    Ok(o.into())
}
pub fn delete_okr(conn: &PgConnection, oid: Uuid) -> Result<(), ApiError> {
    use crate::schema::okrs::dsl::*;
    let _ = diesel::delete(okrs.filter(id.eq(oid.to_string()))).execute(conn)?;
    Ok(())
}

#[cfg(test)]
pub mod tests {
    use crate::database::establish_connection;

    use super::*;
    use chrono::Utc;

    #[test]
    fn test_find_all_okrs() {
        let pool = establish_connection();
        let conn = pool.get().expect("couldn't get db connection from pool");
        let okrs = get_all_okrs(&conn).unwrap();
        assert!(okrs.0.len() > 0);
    }
    #[test]
    fn test_find_okr_by_owner_id() {
        let pool = establish_connection();
        let conn = pool.get().expect("couldn't get db connection from pool");
        let owner_id = Uuid::parse_str("00000000-0000-0000-0000-000000000000").unwrap();
        let okrs = find_okr_by_owner_id(&conn, owner_id).unwrap();
        assert!(okrs.0.len() > 0);
    }

    #[test]
    fn test_find_okr() {
        let pool = establish_connection();
        let conn = pool.get().expect("couldn't get db connection from pool");
        let id = Uuid::parse_str("00000000-0000-0000-0000-000000000010").unwrap();
        let o = find_okr_by_id(&conn, id).unwrap();
        assert_eq!(o.obj, "一季度OKR")
    }

    #[test]
    fn test_find() {
        let pool = establish_connection();
        let conn = pool.get().expect("couldn't get db connection from pool");
        let okrs = get_all_okrs(&conn).unwrap();
        let okr = &okrs.0[0];
        let found_okr = find_okr_by_id(&conn, okr.id).unwrap();
        assert_eq!(&okr.id, &found_okr.id);
    }

    #[test]
    fn it_doesnt_find_an_okr() {
        let pool = establish_connection();
        let conn = pool.get().expect("couldn't get db connection from pool");
        let o_id = Uuid::new_v4();
        let not_found_okr = find_okr_by_id(&conn, o_id);
        assert!(not_found_okr.is_err());
    }

    #[test]
    fn it_creates_an_okr_and_deleted() {
        let pool = establish_connection();
        let conn = pool.get().expect("couldn't get db connection from pool");
        let okr_id = Uuid::new_v4();
        let owner_id = Uuid::parse_str("00000000-0000-0000-0000-000000000000").unwrap();
        let new_okr = Okr {
            id: okr_id.to_string(),
            owner_id: owner_id.to_string(),
            owner_name: "test.user".to_string(),
            title: "test title".to_string(),
            obj: "测试目标".to_string(),
            created_at: Utc::now().naive_utc(),
            krs_json: Some(json!(KrsJson::new(vec![String::from("kr1"), String::from("kr2")]))),
        };
        let created = create_okr(&conn, &new_okr);
        assert!(created.is_ok());
        let unwrapped = created.unwrap();
        let found_okr = find_okr_by_id(&conn, unwrapped.id).unwrap();
        assert_eq!(unwrapped, found_okr);
        delete_okr(&conn, unwrapped.id).unwrap();
        let okr = find_okr_by_id(&conn, unwrapped.id);
        assert!(okr.is_err());
    }

    #[test]
    fn it_updates_an_okr() {
        let pool = establish_connection();
        let conn = pool.get().expect("couldn't get db connection from pool");
        let okrs = get_all_okrs(&conn).unwrap();
        let okr = &okrs.0[0];
        let new_okr = UpdateOkr {
            id: okr.id.to_string(),
            title: "update title".to_string(),
            obj: "一季度OKR".to_string(),
            krs_json: None,
        };
        let updated = update_okr(&conn, &new_okr.into());
        assert!(updated.is_ok());
        let found_okr = find_okr_by_id(&conn, okr.id).unwrap();
        assert_eq!(updated.unwrap().id, found_okr.id);
    }

    #[test]
    fn it_fails_to_update_a_nonexistent_okr() {
        let pool = establish_connection();
        let conn = pool.get().expect("couldn't get db connection from pool");
        let new_okr = UpdateOkr {
            id: Uuid::new_v4().to_string(),
            title: "update title".to_string(),
            obj: "update 目标".to_string(),
            krs_json: None,
        };
        let updated = update_okr(&conn, &new_okr.into());
        assert!(updated.is_err());
    }
}

type Id = usize;

pub struct Organization {
    pub id: Id,
    pub name: String,
    pub dependencies: Box<Vec<Organization>>
}

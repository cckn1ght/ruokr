use crate::auth::{encrypt, verify_password, PrivateClaim};
use crate::errors::ApiError;
use crate::schema::users;
use chrono::{NaiveDateTime, Utc};
use diesel::prelude::*;
use rayon::prelude::*;
use uuid::Uuid;

#[derive(Debug, Deserialize, Serialize, PartialEq)]
pub struct UserResponse {
    pub id: Uuid,
    pub name: String,
    pub username: String,
    pub email: String,
}

impl From<PrivateClaim> for UserResponse {
    fn from(claim: PrivateClaim) -> Self {
        UserResponse {
            id: claim.user_id,
            name: claim.name,
            username: claim.username,
            email: claim.email
        }
    }
}
#[derive(Debug, Deserialize, Serialize, PartialEq)]
pub struct UsersResponse(pub Vec<UserResponse>);

#[derive(
    AsChangeset,
    Clone,
    Debug,
    Serialize,
    Deserialize,
    PartialEq,
    Queryable,
    Identifiable,
    Insertable,
)]
#[table_name = "users"]
pub struct User {
    pub id: String,
    pub name: String,
    pub username: String,
    pub email: String,
    pub password: String,
    pub created_at: NaiveDateTime,
}

#[derive(Clone, Debug, Serialize, Deserialize, AsChangeset)]
#[table_name = "users"]
pub struct UpdateUser {
    pub id: String,
    pub name: String,
    pub email: String,
}
impl From<User> for UserResponse {
    fn from(user: User) -> Self {
        UserResponse {
            id: Uuid::parse_str(&user.id).unwrap(),
            name: user.name.to_string(),
            username: user.username.to_string(),
            email: user.email.to_string(),
        }
    }
}

impl From<Vec<User>> for UsersResponse {
    fn from(users: Vec<User>) -> Self {
        UsersResponse(users.into_par_iter().map(|user| user.into()).collect())
    }
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct NewUser {
    pub id: String,
    pub name: String,
    pub username: String,
    pub email: String,
    pub password: String,
}
impl From<NewUser> for User {
    fn from(user: NewUser) -> Self {
        User {
            id: user.id,
            name: user.name,
            username: user.username,
            email: user.email,
            password: encrypt(&user.password),
            created_at: Utc::now().naive_utc(),
        }
    }
}

//
// #[derive(Clone, Debug, Serialize, Deserialize)]
// pub struct AuthUser {
//     pub id: String,
//     pub email: String,
// }
//
// Get all users
pub fn get_all(conn: &PgConnection) -> Result<UsersResponse, ApiError> {
    use crate::schema::users::dsl::*;
    let all_users = users.load::<User>(conn)?;
    Ok(all_users.into())
}

pub fn find(conn: &PgConnection, user_id: Uuid) -> Result<UserResponse, ApiError> {
    use crate::schema::users::dsl::*;
    let user: User = users.find(user_id.to_string()).first(conn)?;
    Ok(user.into())
}
pub fn find_by_auth(
    conn: &PgConnection,
    uname: &str,
    user_password: &str,
) -> Result<UserResponse, ApiError> {
    use crate::schema::users::dsl::*;
    let user: User = users
        .filter(username.eq(uname.to_string()))
        .first::<User>(conn)
        .map_err(|_| ApiError::Unauthorized("Username not registered".into()))?;
    if verify_password(user_password, user.password.as_str()) {
        Ok(user.into())
    } else {
        Err(ApiError::Unauthorized("Invalid login".into()))
    }
}

pub fn create(conn: &PgConnection, user: &User) -> Result<UserResponse, ApiError> {
    let u: User = diesel::insert_into(users::table)
        .values(user)
        .get_result(conn)?;
    Ok(u.into())
}

pub fn update(conn: &PgConnection, user: &UpdateUser) -> Result<UserResponse, ApiError> {
    use crate::schema::users::dsl::*;
    let u = diesel::update(users.find(&user.id.clone()))
        .set(user)
        .get_result::<User>(conn)?;
    Ok(u.into())
}

pub fn delete(conn: &PgConnection, user_id: Uuid) -> Result<(), ApiError> {
    use crate::schema::users::dsl::*;
    let _ = diesel::delete(users.filter(id.eq(user_id.to_string()))).execute(conn)?;
    Ok(())
}

#[cfg(test)]
pub mod tests {
    use crate::database::establish_connection;

    use super::*;

    #[test]
    fn test_find_all() {
        let pool = establish_connection();
        let conn = pool.get().expect("couldn't get db connection from pool");
        let users = get_all(&conn).unwrap();
        assert!(users.0.len() > 0)
    }

    #[test]
    fn test_find_user() {
        let pool = establish_connection();
        let conn = pool.get().expect("couldn't get db connection from pool");
        let users = get_all(&conn).unwrap();
        let user = &users.0[0];
        let user = find(&conn, user.id).unwrap();
        assert_eq!(user.email, "test@test.com")
    }

    #[test]
    fn test_find() {
        let pool = establish_connection();
        let conn = pool.get().expect("couldn't get db connection from pool");
        let users = get_all(&conn).unwrap();
        let user = &users.0[0];
        let found_user = find(&conn, user.id).unwrap();
        assert_eq!(user, &found_user);
    }

    #[test]
    fn it_doesnt_find_a_user() {
        let pool = establish_connection();
        let conn = pool.get().expect("couldn't get db connection from pool");
        let user_id = Uuid::new_v4();
        let not_found_user = find(&conn, user_id);
        assert!(not_found_user.is_err());
    }

    #[test]
    fn it_creates_a_user_and_deleted() {
        let pool = establish_connection();
        let conn = pool.get().expect("couldn't get db connection from pool");
        let user_id = Uuid::new_v4();
        let new_user = NewUser {
            id: user_id.to_string(),
            name: "test user".to_string(),
            username: "test.create".to_string(),
            email: "test.create@test.com".to_string(),
            password: "123456".to_string(),
        };
        let user: User = new_user.into();
        let created = create(&conn, &user);
        assert!(created.is_ok());
        let unwrapped = created.unwrap();
        let found_user = find(&conn, unwrapped.id).unwrap();
        assert_eq!(unwrapped, found_user);
        delete(&conn, unwrapped.id).unwrap();
        let user = find(&conn, unwrapped.id);
        assert!(user.is_err());
    }

    #[test]
    fn it_updates_a_user() {
        let pool = establish_connection();
        let conn = pool.get().expect("couldn't get db connection from pool");
        let users = get_all(&conn).unwrap();
        let user = &users.0[0];
        let update_user = UpdateUser {
            id: user.id.to_string(),
            name: "TestUpdate".to_string(),
            email: "test@test.com".to_string(),
        };
        let updated = update(&conn, &update_user.into());
        assert!(updated.is_ok());
        let found_user = find(&conn, user.id).unwrap();
        assert_eq!(updated.unwrap(), found_user);
    }

    #[test]
    fn it_fails_to_update_a_nonexistent_user() {
        let pool = establish_connection();
        let conn = pool.get().expect("couldn't get db connection from pool");
        let user_id = Uuid::new_v4();
        let update_user = UpdateUser {
            id: user_id.to_string(),
            name: "TestUpdateFailure".to_string(),
            email: "model-update-failure-test@nothing.org".to_string(),
        };
        let updated = update(&conn, &update_user.into());
        assert!(updated.is_err());
    }
}

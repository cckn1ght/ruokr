-- Your SQL goes here
CREATE TABLE organizations
(
    id SERIAL PRIMARY KEY,
    name VARCHAR NOT NULL,
    parent_id INTEGER
);
CREATE INDEX idx_users_name ON users(name);
CREATE INDEX idx_users_username ON users(username);
CREATE INDEX idx_users_email ON users(email);
INSERT INTO users (id, name, username, email, password) VALUES
('00000000-0000-0000-0000-000000000000','test','test','test@test.com','$2b$04$9mvnaSpiT5fyQQvxkGnZ4uaF2E8MrMshPYITjxIiJx1oFJ2JKL8QO');

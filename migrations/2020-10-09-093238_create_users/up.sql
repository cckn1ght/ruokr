-- Your SQL goes here
CREATE TABLE users
(
    id         varchar(36)  NOT NULL PRIMARY KEY,
    name       varchar(100) NOT NULL,
    username       varchar(100) NOT NULL UNIQUE,
    email      varchar(100) NOT NULL UNIQUE,
    password   varchar(122) NOT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT NOW()
);
CREATE INDEX idx_users_name ON users(name);
CREATE INDEX idx_users_username ON users(username);
CREATE INDEX idx_users_email ON users(email);
INSERT INTO users (id, name, username, email, password) VALUES
('00000000-0000-0000-0000-000000000000','test','test','test@test.com','$2b$04$9mvnaSpiT5fyQQvxkGnZ4uaF2E8MrMshPYITjxIiJx1oFJ2JKL8QO');

CREATE TABLE okrs
(
    id         varchar(36)  NOT NULL PRIMARY KEY,
    owner_id       varchar(36) NOT NULL,
    owner_name       varchar(100) NOT NULL,
    title      varchar(100) NOT NULL,
    obj      varchar(255) NOT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT NOW(),
    krs_json jsonb,
    FOREIGN KEY (owner_id) REFERENCES users(id) ON DELETE CASCADE
);
CREATE INDEX idx_okrs_title ON okrs(title);
CREATE INDEX idx_okrs_obj ON okrs(obj);
INSERT INTO okrs (id, owner_id, owner_name, title, obj, krs_json) VALUES
('00000000-0000-0000-0000-000000000010','00000000-0000-0000-0000-000000000000','test','一季度OKR','一季度OKR', '{"krs": [{"content": "kr1", "progress": 0}, {"content": "kr2", "progress": 0}]}');
